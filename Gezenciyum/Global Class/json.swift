//
//  json.swift
//  testPostJson
//
//  Created by Yunus Tek on 14/10/2017.
//  Copyright © 2017 Yunus Tek. All rights reserved.
//

import Foundation
import UIKit

private let _sharedjson = json()
class json : NSObject  {
    
    // MARK: - SHARED INSTANCE
    class var shared : json {
        return _sharedjson
    }
    
    // TodoSliderList
    struct TodoSliderList: Codable {
        var Id: Int?
        var Image: String?

        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }

        // GET ALL TODOS
        static func endpointForTodos() -> String {
            return GlobalFunctions.shared.apiUrl + "/slider/sliderList"
        }

        static func allTodos(completionHandler: @escaping ([TodoSliderList]?, Error?) -> Void) {
            let endpoint = TodoSliderList.endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared

            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    completionHandler(nil, error)
                    return
                }

                let decoder = JSONDecoder()
                do {
                    let todos = try decoder.decode([TodoSliderList].self, from: responseData)
                    completionHandler(todos, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
    
    // TodoUserRegister
    struct TodoUserRegister: Codable {
        var sonuc: String?
        var error: String?

        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos() -> String {
            return GlobalFunctions.shared.apiUrl + "/user/register"
        }
        
        // POST - SAVE
        func save(Name: String, Surname: String, Email: String, Phone: String, Password: String, Address: String
            , CityId: Int, TownId: Int, IwantEmail: Bool, IwantSms: Bool
            , completionHandler: @escaping ([String: String]?, Error?) -> Void) {
            let todosEndpoint = TodoUserRegister.endpointForTodos()
            guard let todosURL = URL(string: todosEndpoint) else {
                let error = BackendError.urlError(reason: "Could not create URL")
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
        
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            let params =
                ["Name": Name
                    , "Surname":Surname
                    , "Email":Email
                    , "Phone":Phone
                    , "Password":Password
                    , "Address":Address
                    , "CityId":CityId
                    , "TownId":TownId
                    , "IwantEmail":IwantEmail
                    , "IwantSms":IwantSms
//                    , "Latitude":Latitude
//                    , "Longitude":Longitude
                    ] as [String : Any]
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = BackendError.urlError(reason: "Error httpBody")
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let response = response {
                    print(response)
                }
                if let data = data {
                    do {
                        let json: [String: String] = try JSONSerialization.jsonObject(with: data, options: []) as! [String : String]
                        
                        completionHandler(json, nil)
                        
                    } catch {
                        completionHandler(nil, error)
                    }
                }
                }.resume()
        }
    }
    
    
    // TodoUserLogin
    struct TodoUserLogin: Codable {
        
        let userId : Int?
        let address : String?
        let cityName : String?
        let createDate : String?
        let email : String?
        let iwantEmail : Bool?
        let iwantSms : Bool?
        let latitude : Double?
        let longitude : Double?
        let name : String?
        let notificationToken : String?
        let cityId : Int?
        let townId : Int?
        let password : String?
        let phone : String?
        let surname : String?
        let townName : String?
        
        enum CodingKeys: String, CodingKey {
            case userId = "UserId"
            case address = "Address"
            case cityName = "CityName"
            case createDate = "CreateDate"
            case email = "Email"
            case iwantEmail = "IwantEmail"
            case iwantSms = "IwantSms"
            case latitude = "Latitude"
            case longitude = "Longitude"
            case name = "Name"
            case notificationToken = "NotificationToken"
            case cityId = "CityId"
            case townId = "TownId"
            case password = "Password"
            case phone = "Phone"
            case surname = "Surname"
            case townName = "TownName"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            userId = try values.decodeIfPresent(Int.self, forKey: .userId)
            address = try values.decodeIfPresent(String.self, forKey: .address)
            cityName = try values.decodeIfPresent(String.self, forKey: .cityName)
            createDate = try values.decodeIfPresent(String.self, forKey: .createDate)
            email = try values.decodeIfPresent(String.self, forKey: .email)
            iwantEmail = try values.decodeIfPresent(Bool.self, forKey: .iwantEmail)
            iwantSms = try values.decodeIfPresent(Bool.self, forKey: .iwantSms)
            latitude = try values.decodeIfPresent(Double.self, forKey: .latitude)
            longitude = try values.decodeIfPresent(Double.self, forKey: .longitude)
            name = try values.decodeIfPresent(String.self, forKey: .name)
            notificationToken = try values.decodeIfPresent(String.self, forKey: .notificationToken)
            cityId = try values.decodeIfPresent(Int.self, forKey: .cityId)
            townId = try values.decodeIfPresent(Int.self, forKey: .townId)
            password = try values.decodeIfPresent(String.self, forKey: .password)
            phone = try values.decodeIfPresent(String.self, forKey: .phone)
            surname = try values.decodeIfPresent(String.self, forKey: .surname)
            townName = try values.decodeIfPresent(String.self, forKey: .townName)
        }

        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos(_ Email: String, _ Password: String) -> String {
            return GlobalFunctions.shared.apiUrl + "/user/login?Email=\(Email)&Password=\(Password)"
        }
        
        static func allTodos(Email: String, Password: String, completionHandler: @escaping (TodoUserLogin?, Error?) -> Void) {
            let endpoint = TodoUserLogin.endpointForTodos(Email, Password)
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared
            
            session.dataTask(with: urlRequest) { (data, response, error) in
                if let response = response {
                    print(response)
                }
                if let requestdata = data {
                    do {
                        let json: [String: Any] = try JSONSerialization.jsonObject(with: requestdata, options: []) as! [String : Any]
                        
                        if json.keys.first != "error" {
                            let todos = try JSONDecoder().decode(TodoUserLogin.self, from: requestdata)
                            
                            completionHandler(todos, nil)
                        } else {
                            let error = BackendError.urlError(reason: json.values.first as! String)
                            completionHandler(nil, error)
                        }
                        
                    } catch {
                        completionHandler(nil, error)
                    }
                }
                }.resume()
            
        }
    }

    // TodoCityAndTownList
    struct CityList : Codable {
        let Id : Int?
        let Name : String?
        let PlateCode : Int?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            Name = try values.decodeIfPresent(String.self, forKey: .Name)
            PlateCode = try values.decodeIfPresent(Int.self, forKey: .PlateCode)
        }
        
    }
    
    struct TownList : Codable {
        let CityId : Int?
        let Id : Int?
        let Name : String?

        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            CityId = try values.decodeIfPresent(Int.self, forKey: .CityId)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            Name = try values.decodeIfPresent(String.self, forKey: .Name)
        }
        
    }
    
    struct TodoCityAndTownList: Codable {
        
        let cityList : [CityList]?
        let townList : [TownList]?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            cityList = try values.decodeIfPresent([CityList].self, forKey: .cityList)
            townList = try values.decodeIfPresent([TownList].self, forKey: .townList)
        }
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        static func allTodos(completionHandler: @escaping (TodoCityAndTownList?, Error?) -> Void) {
            
                do {
                    if let file = Bundle.main.url(forResource: "CityAndTownList", withExtension: "json") {
                        let data = try Data(contentsOf: file)
                        
                        let decoder = JSONDecoder()
                        do {
                            let todos = try decoder.decode(TodoCityAndTownList.self, from: data)
                            completionHandler(todos, nil)
                            
                        } catch {
                            print("error trying to convert data to JSON")
                            print(error)
                            completionHandler(nil, error)
                        }
                        
                    } else {
                        print("no file")
                        let error = BackendError.urlError(reason: "Bir Hata Olustu")
                        completionHandler(nil, error)
                    }
                } catch {
                    completionHandler(nil, error)
                }
                
            }
    }
    
    // TodoForgetEmail
    struct TodoForgetEmail: Codable {
        var sonuc: String?
        var error: String?
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos(_ email: String) -> String {
            return GlobalFunctions.shared.apiUrl + "/user/forgotPassword?Email=\(email)"
        }
        
        static func allTodos(_ email: String, completionHandler: @escaping (TodoForgetEmail?, Error?) -> Void) {
            let endpoint = TodoForgetEmail.endpointForTodos(email)
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    completionHandler(nil, error)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let todos = try decoder.decode(TodoForgetEmail.self, from: responseData)
                    completionHandler(todos, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
    
    // TodoChangePassword
    struct TodoChangePassword: Codable {
        var sonuc: String?
        var error: String?
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos(_ oldPassword: String, newPassword: String) -> String {
            return GlobalFunctions.shared.apiUrl + "/user/passwordChanged?UserId=\(GlobalFunctions.currentUser!.userId!)&OldPassword=\(oldPassword)&NewPassword=\(newPassword)"
        }
        
        static func allTodos(_ oldPassword: String, newPassword: String, completionHandler: @escaping (TodoChangePassword?, Error?) -> Void) {
            let endpoint = TodoChangePassword.endpointForTodos(oldPassword, newPassword: newPassword)
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    completionHandler(nil, error)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let todos = try decoder.decode(TodoChangePassword.self, from: responseData)
                    completionHandler(todos, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
    
    // TodoProductList
    struct TodoProductList: Codable {
        let CreateDate : String?
        let Description : String?
        let Id : Int?
        let Image : String?
        let Name : String?
        let Price : Double?
        let MyProdQuantity: Int?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            CreateDate = try values.decodeIfPresent(String.self, forKey: .CreateDate)
            Description = try values.decodeIfPresent(String.self, forKey: .Description)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            Image = try values.decodeIfPresent(String.self, forKey: .Image)
            Name = try values.decodeIfPresent(String.self, forKey: .Name)
            Price = try values.decodeIfPresent(Double.self, forKey: .Price)
            MyProdQuantity = try values.decodeIfPresent(Int.self, forKey: .MyProdQuantity)
        }
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos() -> String {
            if let userId = GlobalFunctions.currentUser?.userId {
                return GlobalFunctions.shared.apiUrl + "/product/productList?UserId=\(userId)"
            } else {
                return GlobalFunctions.shared.apiUrl + "/product/productList?UserId=0"
            }
            
        }
        
        static func allTodos(completionHandler: @escaping ([TodoProductList]?, Error?) -> Void) {
            let endpoint = TodoProductList.endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data", error!)
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    print(error!)
                    completionHandler(nil, error)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let todos = try decoder.decode([TodoProductList].self, from: responseData)
                    completionHandler(todos, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
    
    // TodoUserUpdate
    struct TodoUserUpdate: Codable {
        var sonuc: String?
        var error: String?
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos() -> String {
            return GlobalFunctions.shared.apiUrl + "/user/profileUpdate"
        }
        
        // POST - SAVE
        func save(Name: String, Surname: String, Phone: String, Address: String, CityId: Int, TownId: Int, IwantEmail: Bool, IwantSms: Bool
            ,completionHandler: @escaping ([String: String]?, Error?) -> Void) {
            let todosEndpoint = TodoUserUpdate.endpointForTodos()
            guard let todosURL = URL(string: todosEndpoint) else {
                let error = BackendError.urlError(reason: "Could not create URL")
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            let params =
                ["Id": GlobalFunctions.currentUser!.userId!
                    ,"Name": Name
                    , "Surname":Surname
                    , "Phone":Phone
                    , "Address":Address
                    , "CityId":CityId
                    , "TownId":TownId
                    , "IwantEmail":IwantEmail
                    , "IwantSms":IwantSms
//                    , "Latitude":Latitude
//                    , "Longitude":Longitude
                    
                    ] as [String : Any]
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = BackendError.urlError(reason: "Error httpBody")
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let response = response {
                    print(response)
                }
                if let data = data {
                    do {
                        let json: [String: String] = try JSONSerialization.jsonObject(with: data, options: []) as! [String : String]
                        
                        completionHandler(json, nil)
                        
                    } catch {
                        completionHandler(nil, error)
                    }
                }
                }.resume()
        }
        
    }
    
    // TodoShopCardAdd
    struct TodoShopCardAdd: Codable {
        var sonuc: String?
        var error: String?
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos(_ productId: Int) -> String {
            return GlobalFunctions.shared.apiUrl + "/shoppingcard/shopCardAdd?UserId=\(GlobalFunctions.currentUser!.userId!)&ProductId=\(productId)"
        }
        
        static func allTodos(productId: Int, completionHandler: @escaping (TodoShopCardAdd?, Error?) -> Void) {
            let endpoint = TodoShopCardAdd.endpointForTodos(productId)
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    completionHandler(nil, error)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let todos = try decoder.decode(TodoShopCardAdd.self, from: responseData)
                    completionHandler(todos, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
    
    // TodoShopCardRemove
    struct TodoShopCardRemove: Codable {
        var sonuc: String?
        var error: String?
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos(_ productId: Int? = nil, _ removeProduct: Bool = false, _ removeCard: Bool = false) -> String {
            
            switch true {
            case removeProduct:
                return GlobalFunctions.shared.apiUrl + "/shoppingcard/shopCardRemoveAll?UserId=\(GlobalFunctions.currentUser!.userId!)&ProductId=\(productId!)"
            case removeCard:
                return GlobalFunctions.shared.apiUrl + "/shoppingcard/myOrderRemoveAll?UserId=\(GlobalFunctions.currentUser!.userId!)"
            default: // removeCount
                return GlobalFunctions.shared.apiUrl + "/shoppingcard/shopCardRemove?UserId=\(GlobalFunctions.currentUser!.userId!)&ProductId=\(productId!)"
            }
        }
        
        static func allTodos(productId: Int? = nil, removeProduct: Bool = false, removeCard: Bool = false, completionHandler: @escaping (TodoShopCardRemove?, Error?) -> Void) {
            let endpoint = TodoShopCardRemove.endpointForTodos(productId, removeProduct, removeCard)
            
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    completionHandler(nil, error)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let todos = try decoder.decode(TodoShopCardRemove.self, from: responseData)
                    completionHandler(todos, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
    
    
    // TodoCardList
    struct TodoCardList: Codable {
        let Id : Int?
        let ProductId : Int?
        let Quantity : Int?
        let CreateDate : String?
        let Description : String?
        let Name : String?
        let Image : String?
        let Price : Double?
        let TotalPrice : Double?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            ProductId = try values.decodeIfPresent(Int.self, forKey: .ProductId)
            Quantity = try values.decodeIfPresent(Int.self, forKey: .Quantity)
            CreateDate = try values.decodeIfPresent(String.self, forKey: .CreateDate)
            Description = try values.decodeIfPresent(String.self, forKey: .Description)
            Name = try values.decodeIfPresent(String.self, forKey: .Name)
            Image = try values.decodeIfPresent(String.self, forKey: .Image)
            Price = try values.decodeIfPresent(Double.self, forKey: .Price)
            TotalPrice = try values.decodeIfPresent(Double.self, forKey: .TotalPrice)
        }
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos() -> String {
            return GlobalFunctions.shared.apiUrl + "/order/shopCardList?UserId=\(GlobalFunctions.currentUser!.userId!)"
        }
        
        static func allTodos(completionHandler: @escaping ([TodoCardList]?, Error?) -> Void) {
            let endpoint = TodoCardList.endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    completionHandler(nil, error)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let todos = try decoder.decode([TodoCardList].self, from: responseData)
            
                    completionHandler(todos, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
    
    // TodoShopCardToOrder
    struct TodoShopCardToOrder: Codable {
        var sonuc: String?
        var error: String?
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos() -> String {
            return GlobalFunctions.shared.apiUrl + "/order/shopCardToOrder?UserId=\(GlobalFunctions.currentUser!.userId!)"
        }
        
        static func allTodos(completionHandler: @escaping (TodoShopCardToOrder?, Error?) -> Void) {
            let endpoint = TodoShopCardToOrder.endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    completionHandler(nil, error)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let todos = try decoder.decode(TodoShopCardToOrder.self, from: responseData)
                    completionHandler(todos, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
    
    
    // TodoLastOrderList
    struct OrderProducts : Codable {
        let Id : Int?
        let Quantity : Int?
        let Description : String?
        let Image : String?
        let Name : String?
        let Price : Double?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            Quantity = try values.decodeIfPresent(Int.self, forKey: .Quantity)
            Description = try values.decodeIfPresent(String.self, forKey: .Description)
            Image = try values.decodeIfPresent(String.self, forKey: .Image)
            Name = try values.decodeIfPresent(String.self, forKey: .Name)
            Price = try values.decodeIfPresent(Double.self, forKey: .Price)
        }
    }
    
    struct TodoLastOrderList: Codable {
        let OrderCode : String?
        let Id : Int?
        let TotalPrice : String?
        let CreateDate : String?
        let OrderStatusId : Int?
        let Name : String?
        let orderProducts : [OrderProducts]?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            OrderCode = try values.decodeIfPresent(String.self, forKey: .OrderCode)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            TotalPrice = try values.decodeIfPresent(String.self, forKey: .TotalPrice)
            CreateDate = try values.decodeIfPresent(String.self, forKey: .CreateDate)
            OrderStatusId = try values.decodeIfPresent(Int.self, forKey: .OrderStatusId)
            Name = try values.decodeIfPresent(String.self, forKey: .Name)
            orderProducts = try values.decodeIfPresent([OrderProducts].self, forKey: .orderProducts)
        }
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
        
        // GET ALL TODOS
        static func endpointForTodos() -> String {
            return GlobalFunctions.shared.apiUrl + "/order/lastOrders?UserId=\(GlobalFunctions.currentUser!.userId!)"
        }
        
        static func allTodos(completionHandler: @escaping ([TodoLastOrderList]?, Error?) -> Void) {
            let endpoint = TodoLastOrderList.endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                let error = BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {
                (data, response, error) in
                guard let responseData = data else {
                    print("Error: did not receive data")
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    completionHandler(nil, error)
                    return
                }
                
                let decoder = JSONDecoder()
                do {
                    let todos = try decoder.decode([TodoLastOrderList].self, from: responseData)
                    completionHandler(todos, nil)
                } catch {
                    print("error trying to convert data to JSON")
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }


}

extension UIImageView {
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
//        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else {
//            DispatchQueue.main.async() {
//                self.image = UIImage(named: "logo")
//            }
            return
        }
        downloadedFrom(url: url, contentMode: mode)
    }
    
}


