//
//  GlobalFunctions.swift
//  Orca POS Mobile
//
//  Created by Yunus TEK on 21.03.2017.
//  Copyright © 2017 Orca Businesss Solutions. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import AudioToolbox

private let _sharedGlobalFunctions = GlobalFunctions()
class GlobalFunctions : NSObject, URLSessionDelegate {
    
    // MARK: - SHARED INSTANCE
    class var shared : GlobalFunctions {
        return _sharedGlobalFunctions
    }
    
    let apiUrl = "http://gezenciyumapi.loopbs.com/api"
    
    static var sliderList: [json.TodoSliderList]? = nil
    static var cityAndTownList: json.TodoCityAndTownList? = nil
    static var currentUser: json.TodoUserLogin? = nil
    static var productList: [json.TodoProductList]? = nil
    static var selectedProduct: json.TodoProductList? = nil
    static var cardList: [json.TodoCardList]? = nil
    
    // Last Order
    static var lastOrder: [json.TodoLastOrderList]? = nil
    static var activeOrder: [json.TodoLastOrderList]? = nil
    static var oldOrder: [json.TodoLastOrderList]? = nil

    static var formType = "Kisisel"    
    
    class func openActivity() {
        // Hidden navigationbar
        GlobalFunctions.getTopController().navigationController?.isNavigationBarHidden = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        // Open
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ActivityView")
        GlobalFunctions.getTopController().addChildViewController(vc)
        vc.view.frame = GlobalFunctions.getTopController().view.frame
        GlobalFunctions.getTopController().view.addSubview(vc.view)
        vc.didMove(toParentViewController: GlobalFunctions.getTopController())
    }
    
    class func closeActivity() {
            // if it is open, close it
            if let cvc = GlobalFunctions.getTopController().childViewControllers.last {
                cvc.view.backgroundColor = UIColor.clear
                cvc.view.isHidden = true
                cvc.view.alpha = 0
                cvc.willMove(toParentViewController: nil)
                cvc.view.removeFromSuperview()
                cvc.removeFromParentViewController()
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                // Hidden navigationbar
                GlobalFunctions.getTopController().navigationController?.isNavigationBarHidden = false
            }
    }
    
    func getCity(_ cityName: String) -> json.CityList? {
        var city: [json.CityList]?
        city = GlobalFunctions.cityAndTownList!.cityList!.filter() {
            city in
            return (city.Name == cityName) // Arrayde varsa
        }
        
        if city!.count != 0 {
            return city![0]
        }
        return nil
    }
    
    func getTown(_ townName: String) -> json.TownList? {
        var town: [json.TownList]?
        town = GlobalFunctions.cityAndTownList!.townList!.filter() {
            town in
            return (town.Name == townName) // Arrayde varsa
        }
        
        if town!.count != 0 {
            return town![0]
        }
        return nil
    }
    
    class func isValidEmail(_ email:String) -> Bool {
        print("validate emilId: \(email)")

        let emailRegEx =
            "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
                + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
                + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
                + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
                + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
                + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
                + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
//        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)

        let result = emailTest.evaluate(with: email)
        
        if result == false {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Doğru E-Posta adresi girmelisiniz")
        }
        
        return result
    }
    
    class func isValidPhone(_ value: String) -> Bool {
        
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        var result =  phoneTest.evaluate(with: value)
        
        if value.count != 13 {
            result = false
        }
        
        if result == false {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Doğru Telefon numarası girmelisiniz")
        }        
        
        return result
    }
    
    class func isValidPasswordConfirm(_ password: String , confirmPassword : String) -> Bool {
        if password == confirmPassword {
            return true
        } else {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Şifre doğrulama başarısız")
            return false
        }
    }
    
    class func isValidPasswordLong(_ password: String) -> Bool {
        if password.count >= 6{
            return true
        } else {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Parolanız 6 karakterden uzun olmalıdır")
            return false
        }
    }
    
    class func getTopController() -> UIViewController {
        
        var topController = UIApplication.shared.keyWindow!.rootViewController! as UIViewController
        
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        
        return topController
    }
    
}

extension UIViewController {
    func hideKeyboardWhenTappedAround(){
        // Tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(LoginVC.dismissKeyBoard)) // yazılan viewin bir önemi yok.
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyBoard(_ sender: UITapGestureRecognizer? = nil){
        view.endEditing(true)
    }
}

extension UIButton {
    func setBorderColor(_ borderWidth: CGFloat = 1, _ borderColor: CGColor = UIColor.blue.cgColor) {
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
    }
}

extension UILabel {
    func setBorderColor(_ borderWidth: CGFloat = 1, _ borderColor: CGColor = UIColor.blue.cgColor) {
        self.layer.borderColor = borderColor
        self.layer.borderWidth = borderWidth
    }
}
