//
//  AnaSayfaVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 25.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class AnaSayfaVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fillScrollView()
        presentTransparentNavigationBar()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillScrollView () {
        
        scrollView.isPagingEnabled = true
        var countIndex = CGFloat(0)
        if let sliderList = GlobalFunctions.sliderList {
            for list in sliderList {
                let image = UIImageView()
                image.frame = CGRect(x: countIndex * view.bounds.size.width, y: 0, width: view.bounds.size.width, height: scrollView.frame.height)
                
                image.image = nil
                if let imageUrl = list.Image {
                    DispatchQueue.main.async {
                        image.downloadedFrom(link: imageUrl)
                    }
                }
                
                countIndex += 1
                image.contentMode = .scaleToFill
                scrollView.addSubview(image)
            }
        }
        
        scrollView.contentSize = CGSize(width: CGFloat(countIndex) * view.bounds.size.width, height: scrollView.frame.height)
        
        if GlobalFunctions.cardList != nil {
            // BadgeValue
            if let tabItems = self.tabBarController!.tabBar.items as NSArray?
            {
                let tabItem = tabItems[2] as! UITabBarItem
                tabItem.badgeValue = String(GlobalFunctions.cardList!.count)
            }
        }
    }
    
    
    func presentTransparentNavigationBar() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        self.navigationItem.hidesBackButton = true
        //        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
        //        self.navigationController!.navigationBar.barTintColor = GlobalFunctions.shared.getColor("black")
    }

}
