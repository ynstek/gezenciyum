//
//  FormVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 27.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class FormVC: UIViewController {

    @IBOutlet weak var textViewKisisel: UITextView!
    @IBOutlet weak var textViewSozlesme: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textViewSozlesme.isHidden = true
        textViewKisisel.isHidden = true
        
        if GlobalFunctions.formType == "Kisisel" {
            self.textViewKisisel.isHidden = false
        } else if GlobalFunctions.formType == "Sozlesme" {
            self.textViewSozlesme.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
