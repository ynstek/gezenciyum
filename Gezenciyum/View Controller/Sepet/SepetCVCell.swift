//
//  SepetCVCell.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 3.12.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class SepetCVCell: UICollectionViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var qty: UILabel!
    
    @IBOutlet weak var Id: UILabel!
    @IBOutlet weak var ProductId: UILabel!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnMinus: UIButton!
    @IBOutlet weak var border: UILabel!
    
    @IBAction func btnPlus(_ sender: Any) {
        GlobalFunctions.openActivity()
        // TodoShopCardAdd
        json.TodoShopCardAdd.allTodos(productId: Int(ProductId.text!)!) { (result, error) in
            
            if error == nil {
                if result!.sonuc != nil {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadCard"), object: nil)
                        GlobalFunctions.closeActivity()
                    }

                    
                } else if let errorMessage = result!.error {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                    }
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: errorMessage)
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                    }
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                }
                
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    GlobalFunctions.closeActivity()
                }
                print("Error ", error!)
                AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
            }
        }
    }
    
    @IBAction func btnMinus(_ sender: Any) {
        if qty.text != "1" {
            GlobalFunctions.openActivity()
            // TodoShopCardAdd
            json.TodoShopCardRemove.allTodos(productId: Int(ProductId.text!)!) { (result, error) in
                
                if error == nil {
                    if result!.sonuc != nil {
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadCard"), object: nil)
                            GlobalFunctions.closeActivity()

                        }
                        

                        
                    } else if let errorMessage = result!.error {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: errorMessage)
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                    }
                    
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                    }
                    print("Error ", error!)
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                }
            }
        }
    }
    
    @IBAction func btnDelete(_ sender: Any) {
        AlertFunctions.messageType.showYesNoAlert("Ürün Silme", bodyMessage: "'" + self.name.text! + "' ürününüz sepetten silinecek, emin misiniz?", {
            
            GlobalFunctions.openActivity()
            // TodoShopCardAdd
            json.TodoShopCardRemove.allTodos(productId: Int(self.ProductId.text!)!, removeProduct: true) { (result, error) in
                
                if error == nil {
                    if result!.sonuc != nil {
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadCard"), object: nil)
                            GlobalFunctions.closeActivity()
                        }
                        
                        
                    } else if let errorMessage = result!.error {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: errorMessage)
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                    }
                    
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                    }
                    print("Error ", error!)
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                }
            }
            
        }, no: {
            
        })
        
    }
    
}
