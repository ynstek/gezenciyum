//
//  SepetVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 25.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class SepetVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtTotalPrice: UILabel!
    @IBOutlet weak var btnSepetiOnayla: UIButton!
    @IBOutlet weak var txtSepetiOnayla: UILabel!
    
    @IBOutlet weak var btnRemoveOrders: UIButton!
    
    
    var segueForButton = "products"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presentTransparentNavigationBar()
        
        NotificationCenter.default.addObserver(self, selector: #selector(SepetVC.reloadCard(_:)), name: NSNotification.Name(rawValue: "reloadCard"), object: nil)
        
        self.btnRemoveOrders.setBorderColor(3, UIColor.red.cgColor)
    }
    
    @objc func reloadCard (_ notification: Notification) {
        getCardList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if GlobalFunctions.currentUser != nil {
            getCardList()
        } else {
            openLoginView()
        }
    }
    
    func getCardList () {
        GlobalFunctions.openActivity()
        self.collectionView.allowsSelection = false
        print("Start Card List Update.")
        // CardList
        json.TodoCardList.allTodos { (result, error) in
            if error == nil {
                print("Card List Updated.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    GlobalFunctions.cardList = result
                    self.collectionView.reloadData()
                    self.collectionView.allowsSelection = true

                    if GlobalFunctions.cardList?.count != 0 {
                        var totalPrice: Double = 0
                        for order in GlobalFunctions.cardList! {
                            totalPrice = totalPrice + order.TotalPrice!
                        }
                        self.txtTotalPrice.text = String(totalPrice) + " TL"
                        self.txtSepetiOnayla.text = "Sepeti Onayla >"
                        self.segueForButton = "orders"
                    } else {
                        self.txtTotalPrice.text = "0.00 TL"
                        self.txtSepetiOnayla.text = "Sepetinizde ürün yok"
                        self.segueForButton = "products"
                    }
                    
                    // BadgeValue
                    if let tabItems = self.tabBarController?.tabBar.items as NSArray!
                    {
                        let tabItem = tabItems[2] as! UITabBarItem
                        tabItem.badgeValue = String(result!.count)
                    }
                    
                    GlobalFunctions.closeActivity()

                }
                
            } else {
                print("CardList", error!)
            }
        }
    }
    
    // MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = GlobalFunctions.cardList?.count {
            return count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sepetCell", for: indexPath) as! SepetCVCell
        
        cell.image.image = nil
        DispatchQueue.main.async {
            cell.image.downloadedFrom(link: GlobalFunctions.cardList![indexPath.row].Image!)
            cell.activity.stopAnimating()
        }
        
        cell.name.text = GlobalFunctions.cardList![indexPath.row].Name
        cell.price.text = "1 x " + String(GlobalFunctions.cardList![indexPath.row].Price!) + " TL"
        cell.desc.text = GlobalFunctions.cardList![indexPath.row].Description
        cell.totalPrice.text = String(GlobalFunctions.cardList![indexPath.row].TotalPrice!) + " TL"
        cell.Id.text = String(GlobalFunctions.cardList![indexPath.row].Id!)
        cell.ProductId.text = String(GlobalFunctions.cardList![indexPath.row].ProductId!)
        
        cell.qty.text = String(describing: GlobalFunctions.cardList![indexPath.row].Quantity!)
        cell.qty.setBorderColor(1, UIColor.lightGray.cgColor)
//        cell.btnPlus.setBorderColor(1, UIColor.lightGray.cgColor)
//        cell.btnMinus.setBorderColor(1, UIColor.lightGray.cgColor)
        cell.border.setBorderColor(1, UIColor.lightGray.cgColor)
        
        return cell
    }
    
    
    func openLoginView(){
        let UpdatePopover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login") as! UINavigationController
        UpdatePopover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        UpdatePopover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = UpdatePopover.popoverPresentationController
        popoverCont?.delegate = self // add UIPopoverPresentationControllerDelegate
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(UpdatePopover, animated: true, completion: nil)
    }
    
    @IBAction func btnRemoveCart(_ sender: Any) {
        AlertFunctions.messageType.showYesNoAlert("Sepeti Boşaltma", bodyMessage: "Sepetinizdeki tüm ürünler silinecek, emin misiniz?", {
            
            GlobalFunctions.openActivity()
            // TodoShopCardAdd
            json.TodoShopCardRemove.allTodos(productId: 0, removeCard: true) { (result, error) in
                
                if error == nil {
                    if result!.sonuc != nil {
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadCard"), object: nil)
                            GlobalFunctions.closeActivity()
                        }
                        
                    } else if let errorMessage = result!.error {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: errorMessage)
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                    }
                    
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    GlobalFunctions.closeActivity()
                    }
                    print("Error ", error!)
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                }
            }
            
        }, no: {
            
        })
    }
    
    @IBAction func btnSepetiOnayla(_ sender: Any) {
        AlertFunctions.messageType.showOKAlertVoid("Sipariş Oluşturuldu", bodyMessage: "Siparişiniz bize ulaştı şuan hazırlanıyor.\nİptal etmek isterseniz iletişim bölümünden bize ulaşabilirsiniz. Keyifli Alışverişler.", {
            
            GlobalFunctions.openActivity()
            // TodoShopCardToOrder
            json.TodoShopCardToOrder.allTodos() { (result, error) in
                
                if error == nil {
                    if result!.sonuc != nil {
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadCard"), object: nil)
                            GlobalFunctions.productList = nil
                            GlobalFunctions.closeActivity()
                            self.performSegue(withIdentifier: "orders", sender: self as AnyObject)
                            
                        }
                        
                    } else if let errorMessage = result!.error {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: errorMessage)
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                    }
                    
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                    }
                    print("Error ", error!)
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                }
            }
        })
    }
    
    
    
    func presentTransparentNavigationBar() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        self.navigationItem.hidesBackButton = true
        //        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
        //        self.navigationController!.navigationBar.barTintColor = GlobalFunctions.shared.getColor("black")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
