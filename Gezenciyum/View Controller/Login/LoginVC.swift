//
//  LoginVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 25.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class LoginVC: UIViewController,UIPopoverPresentationControllerDelegate, UITextFieldDelegate {

    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var btnUyeOl: UIButton!
    @IBOutlet weak var btnSifremiUnuttum: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presentTransparentNavigationBar()
        hideKeyboardWhenTappedAround()
        
//        btnUyeOl = GlobalFunctions.borderColor(btnUyeOl)
//        btnSifremiUnuttum = GlobalFunctions.borderColor(btnSifremiUnuttum)
        
        self.btnUyeOl.setBorderColor()
        self.btnSifremiUnuttum.setBorderColor()
        
        if let email = UserDefaults.standard.object(forKey: "email") as? String {
            txtEmail.text = email
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnDevam(_ sender: Any?) {
        // Trim Last Spaces
        txtEmail.text = txtEmail.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        txtPassword.text = txtPassword.text?.trimmingCharacters(in: CharacterSet.whitespaces)
        
        if !txtEmail.text!.isEmpty && !txtPassword.text!.isEmpty {
            if GlobalFunctions.isValidEmail(txtEmail.text!) {
                // Json User Control
                if Reachability.isConnectedToNetwork() {
                    GlobalFunctions.openActivity()
                    json.TodoUserLogin.allTodos(Email: txtEmail.text!, Password: txtPassword.text!) { (result, error) in
                        
                        if error == nil {
                            
                            GlobalFunctions.currentUser = result
                            
                            UserDefaults.standard.set(result?.email, forKey: "email")
                            UserDefaults.standard.set(result?.password, forKey: "password")
                            UserDefaults.standard.synchronize()
                            
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                GlobalFunctions.closeActivity()
                                self.openHomeView()
                                AlertFunctions.messageType.showInfoAlert("Giriş Başarılı", bodyMessage: "")
                            }
                            
                            // Products
                            json.TodoProductList.allTodos { (result, error) in
                                if error == nil {
                                    GlobalFunctions.productList = result
                                    print("ProductsList updated.")
                                } else {
                                    print("ProductsList", error!)
                                }
                            }
                            
                            // CardList
                            json.TodoCardList.allTodos { (result, error) in
                                if error == nil {
                                    GlobalFunctions.cardList = result
                                    // BadgeValue
                                    if let tabItems = self.tabBarController?.tabBar.items as NSArray!
                                    {
                                        let tabItem = tabItems[2] as! UITabBarItem
                                        tabItem.badgeValue = String(GlobalFunctions.cardList!.count)
                                    }
                                    
                                } else {
                                    print("CardList", error!)
                                }
                            }
                            
                        } else {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                GlobalFunctions.closeActivity()
                                AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "E-Posta veya şifreniz hatalı")
                            }
                            print("Login: ", error!)
                        }
                        
                    }
                } else {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                }
            }
        } else {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "E-Posta veya şifreniz boş")
        }
        
    }
    
    
    @IBAction func btnUyeOlmadanDevam(_ sender: Any) {
        openHomeView()
    }
    
    func openHomeView(){
        let UpdatePopover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Home") as! UITabBarController
        UpdatePopover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        UpdatePopover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = UpdatePopover.popoverPresentationController
        popoverCont?.delegate = self // add UIPopoverPresentationControllerDelegate
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(UpdatePopover, animated: true, completion: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    func presentTransparentNavigationBar() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        self.navigationItem.hidesBackButton = true
        //        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
        //        self.navigationController!.navigationBar.barTintColor = GlobalFunctions.shared.getColor("black")

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Klavyedeki Return butonunu aktif eder
        textField.resignFirstResponder()
        
        switch "" {
        case txtEmail.text!:
            txtEmail.becomeFirstResponder()
        case txtPassword.text!:
            txtPassword.becomeFirstResponder()
        default:
            btnDevam(nil)
            
        }
        
        return true
    }
    
}
