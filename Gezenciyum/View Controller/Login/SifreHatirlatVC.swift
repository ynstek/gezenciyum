//
//  SifreHatirlatVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 28.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class SifreHatirlatVC: UIViewController {

    @IBOutlet weak var txtEmail: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        
        if let email = UserDefaults.standard.object(forKey: "email") as? String {
            txtEmail.text = email
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnSifreHatirlat(_ sender: Any) {
        if GlobalFunctions.isValidEmail(txtEmail.text!) {
            GlobalFunctions.openActivity()
            // TodoForgetEmail
            json.TodoForgetEmail.allTodos(txtEmail.text!) { (result, error) in
                GlobalFunctions.closeActivity()
                if error == nil {
                    if let sonuc = result!.sonuc {
                        AlertFunctions.messageType.showOKAlert(sonuc, bodyMessage: "")
                    } else if let errorMessage = result!.error {
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: errorMessage)
                    } else {
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                    }
                    
                } else {
                    print("Error ForgetEmail", error!)
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                }
            }
            
        }
    }
 
}
