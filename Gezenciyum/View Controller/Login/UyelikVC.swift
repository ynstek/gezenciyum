//
//  UyelikVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 27.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class UyelikVC: UIViewController, UIPopoverPresentationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var txtAd: UITextField!
    @IBOutlet weak var txtSoyad: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtSifre: UITextField!
    @IBOutlet weak var txtSifreTekrar: UITextField!
    
    @IBOutlet weak var swKisiselVeri: UISwitch!
    @IBOutlet weak var swSozlesme: UISwitch!
    
    @IBOutlet weak var swEpostaBilgi: UISwitch!
    @IBOutlet weak var swSmsBilgi: UISwitch!
    
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var txtTown: UITextField!
    @IBOutlet weak var txtAddress: UITextView!
    
    var cityPickerView : UIPickerView!
    var townPickerView : UIPickerView!
    var currentTowns: [json.TownList]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addDoneButtonOnKeyboard()
        hideKeyboardWhenTappedAround()
        txtTown.isEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func UserRegister () {
        // POST JSON
        if Reachability.isConnectedToNetwork() {
            GlobalFunctions.openActivity()

            // Register
            json.TodoUserRegister().save(Name: txtAd.text!, Surname: txtSoyad.text!, Email: txtEmail.text!, Phone: txtPhone.text!, Password: txtSifre.text!
            , Address: txtAddress.text!, CityId: GlobalFunctions.shared.getCity(txtCity.text!)!.Id!, TownId: GlobalFunctions.shared.getTown(txtTown.text!)!.Id!, IwantEmail: swEpostaBilgi.isOn, IwantSms: swSmsBilgi.isOn) { (result, error) in
                GlobalFunctions.closeActivity()
                if error != nil {
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: error!.localizedDescription)
                } else {
                    if result?.keys.first == "error" {
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: result!.values.first!)
                    } else { // "sonuc"
                        self.openHomeView()
                        AlertFunctions.messageType.showOKAlert("Üyeliğini Oluşturuldu", bodyMessage: "Profil sayfanızdan giriş yapabilirsiniz")
                    }
                }
            }
        } else {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    @IBAction func btnDevam(_ sender: Any) {
        if isValid() {
            
            // Json Uye ekleme
            self.UserRegister()

        }
    }
    
    func openHomeView(){
        let UpdatePopover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Home") as! UITabBarController
        UpdatePopover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        UpdatePopover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = UpdatePopover.popoverPresentationController
        popoverCont?.delegate = self // add UIPopoverPresentationControllerDelegate
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(UpdatePopover, animated: true, completion: nil)
    }
    
    @IBAction func ChangePhoneText(_ sender: Any) {
        if (txtPhone.text?.isEmpty)! {
            self.txtPhone.text! = "+90"
        }
    }
    
    func isValid () -> Bool {
        // Bos
        if !txtAd.text!.isEmpty && !txtSoyad.text!.isEmpty
            && !txtEmail.text!.isEmpty && !txtPhone.text!.isEmpty
            && !txtSifre.text!.isEmpty && !txtSifreTekrar.text!.isEmpty
            && swKisiselVeri.isOn && swSozlesme.isOn && !txtAddress.text!.isEmpty
        && GlobalFunctions.shared.getCity(txtCity.text!) != nil && GlobalFunctions.shared.getTown(txtTown.text!) != nil
        {
            
            // Email, Phone, Password check
            if GlobalFunctions.isValidPasswordLong(txtSifre.text!)
                && GlobalFunctions.isValidPasswordConfirm(txtSifre.text!, confirmPassword: txtSifreTekrar.text!)
                && GlobalFunctions.isValidEmail(txtEmail.text!)
                && GlobalFunctions.isValidPhone(txtPhone.text!)
            {
                return true
            } else {
                return false
            }
            
        } else {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Lütfen tüm bilgileri doldurduğunuzdan ve Satış sözleşmesini onayladığınızdan emin olunuz.")
            return false
        }
    }
    
    // Fill City And Town
    func getTownList() -> [json.TownList]? {
        var towns: [json.TownList] = []
        
        towns = towns + GlobalFunctions.cityAndTownList!.townList!.filter() {
            t in
            return (t.CityId == GlobalFunctions.shared.getCity(txtCity.text!)?.Id) // Arrayde varsa
        }
        
        currentTowns = towns
        return currentTowns
    }
    
    func CityListBar(_ textField : UITextField) {
        // UIPickerView
        self.cityPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.cityPickerView.delegate = self
        self.cityPickerView.dataSource = self
        self.cityPickerView.backgroundColor = UIColor.white
        textField.inputView = self.cityPickerView
        
        // ToolBar
        textField.inputAccessoryView = createToolbar()
    }
    
    func TownListBar(_ textField : UITextField) {
        // UIPickerView
        self.townPickerView = UIPickerView(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.townPickerView.delegate = self
        self.townPickerView.dataSource = self
        self.townPickerView.backgroundColor = UIColor.white
        textField.inputView = self.townPickerView
        
        // ToolBar
        textField.inputAccessoryView = createToolbar()
    }
    
    func createToolbar() -> UIToolbar {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Tamam", style: .plain, target: self, action: #selector(UyelikVC.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        return toolBar
    }
    
    // MARK:- Done Button
    @objc func doneClick() {
        dismissKeyBoard()
    }
    
    // MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == cityPickerView {
            return GlobalFunctions.cityAndTownList!.cityList!.count
        } else if pickerView == townPickerView {
            return currentTowns!.count
        }
        
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == cityPickerView {
            return GlobalFunctions.cityAndTownList!.cityList![row].Name
        } else if pickerView == townPickerView {
            return currentTowns![row].Name
        }
        return nil
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == cityPickerView {
            self.txtCity.text = GlobalFunctions.cityAndTownList!.cityList![row].Name
            self.txtTown.isEnabled = false
            txtTown.text = ""

            DispatchQueue.main.async() {
                self.currentTowns = self.getTownList()
                self.txtTown.isEnabled = true
            }
            
        } else if pickerView == townPickerView {
            self.txtTown.text = currentTowns![row].Name
        }
    }
    
    //MARK:- TextFiled Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtCity {
            self.CityListBar(txtCity)
            if self.txtCity.text!.isEmpty {
                self.txtCity.text = GlobalFunctions.cityAndTownList!.cityList![0].Name
                
                self.txtTown.isEnabled = false
                txtTown.text = ""
                DispatchQueue.main.async() {
                    self.currentTowns = self.getTownList()
                    self.txtTown.isEnabled = true
                }
            }
        } else if textField == txtTown {
            self.TownListBar(txtTown)
            if self.txtTown.text!.isEmpty {
                self.txtTown.text = currentTowns![0].Name
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50) )
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Tamam", style: UIBarButtonItemStyle.done, target: self, action: #selector(UyelikVC.doneClick))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        self.txtAd.inputAccessoryView = doneToolbar
        self.txtSoyad.inputAccessoryView = doneToolbar
        self.txtEmail.inputAccessoryView = doneToolbar
        self.txtPhone.inputAccessoryView = doneToolbar
        self.txtAddress.inputAccessoryView = doneToolbar
        self.txtSifre.inputAccessoryView = doneToolbar
        self.txtSifreTekrar.inputAccessoryView = doneToolbar
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Klavyedeki Return butonunu aktif eder
        textField.resignFirstResponder()
        
        switch "" {
        case txtAd.text!:
            txtAd.becomeFirstResponder()
        case txtSoyad.text!:
            txtSoyad.becomeFirstResponder()
        case txtEmail.text!:
            txtEmail.becomeFirstResponder()
        case txtPhone.text!:
            txtPhone.becomeFirstResponder()
        case txtSifre.text!:
            txtSifre.becomeFirstResponder()
        case txtSifreTekrar.text!:
            txtSifreTekrar.becomeFirstResponder()
        case txtCity.text!:
            txtCity.becomeFirstResponder()
        case txtTown.text!:
            txtTown.becomeFirstResponder()
        case txtAddress.text!:
            txtAddress.becomeFirstResponder()
        default: break
            //
        }
        
        return true
    }
    @IBAction func btnKisisel(_ sender: Any) {
        GlobalFunctions.formType = "Kisisel"
    }
    
    @IBAction func btnSozlesme(_ sender: Any) {
        GlobalFunctions.formType = "Sozlesme"
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
}







