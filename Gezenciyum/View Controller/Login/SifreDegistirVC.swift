//
//  SifreDegistirVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 2.12.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class SifreDegistirVC: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var txtEskiSifre: UITextField!
    @IBOutlet weak var txtYeniSifre: UITextField!
    @IBOutlet weak var txtYeniSifreTekrar: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnDegisiklikleriKaydet(_ sender: Any) {
        if isValid() {
            GlobalFunctions.openActivity()
            // TodoChangePassword
            json.TodoChangePassword.allTodos(txtEskiSifre.text!, newPassword: txtYeniSifre.text!) { (result, error) in
                GlobalFunctions.closeActivity()

                if error == nil {
                    if let sonuc = result!.sonuc {
                        self.openHomeView()
                        AlertFunctions.messageType.showOKAlert("Başarılı", bodyMessage: sonuc)

                    } else if let errorMessage = result!.error {
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: errorMessage)
                    } else {
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                    }
                    
                } else {
                    print("Error Forget Email", error!)
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                }
            }
        }
    }
    
    func isValid() -> Bool {
        if !txtEskiSifre.text!.isEmpty && !txtYeniSifre.text!.isEmpty && !txtYeniSifreTekrar.text!.isEmpty {
            
            if txtEskiSifre.text! == GlobalFunctions.currentUser!.password {
                if txtYeniSifre.text! == txtYeniSifreTekrar.text!  {
                    return true
                } else {
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Şifre tekrarı uyuşmuyor. Lütfen tekrar giriniz.")
                }
                
            } else {
                AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Eski Şifreniz yanlış. Lütfen tekrar giriniz.")
                txtEskiSifre.text = ""
            }
            
        } else {
            AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Şifrenizi giriniz")

        }
        
        return false
    }
    
    func openHomeView(){
        let UpdatePopover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Home") as! UITabBarController
        UpdatePopover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        UpdatePopover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = UpdatePopover.popoverPresentationController
        popoverCont?.delegate = self // add UIPopoverPresentationControllerDelegate
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(UpdatePopover, animated: true, completion: nil)
    }
    
}
