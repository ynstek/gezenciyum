//
//  OpenVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 28.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class OpenVC: UIViewController {

    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        // GET JSON
        if Reachability.isConnectedToNetwork(false) {
            
            // Json User Control
            if let mail = UserDefaults.standard.object(forKey: "email") as? String, let password = UserDefaults.standard.object(forKey: "password") as? String {
                
                json.TodoUserLogin.allTodos(Email: mail, Password: password) { (result, error) in
                    if error == nil {
                        GlobalFunctions.currentUser = result
                        
                        // Products
                        json.TodoProductList.allTodos { (result, error) in
                            if error == nil {
                                GlobalFunctions.productList = result
                            } else {
                                print("ProductsList", error!)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                self.label.text = "Bir Hata oluştu.\nDaha sonra tekrar deneyiniz.\nProductsList \(error!.localizedDescription)"
                                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                                    self.activity.stopAnimating()
                                }
                            }
                        }
                        
                        // CardList
                        json.TodoCardList.allTodos { (result, error) in
                            if error == nil {
                                GlobalFunctions.cardList = result
                            } else {
                                print("CardList", error!)
                            }
                        }
                        
//                        self.open()
//                        print("User: ", result!)
                    } else {
//                        self.open()
                        print("Login: ", error!)
                    }
                }
            } else {
                GlobalFunctions.currentUser = nil
                // Products
                json.TodoProductList.allTodos { (result, error) in
                    if error == nil {
                        GlobalFunctions.productList = result
//                        self.open()
                    } else {
                        print("ProductsList", error!)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.label.text = "Bir Hata oluştu.\nDaha sonra tekrar deneyiniz.\nProductsList \(error!.localizedDescription)"
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.activity.stopAnimating()
                        }
                    }
                }
            }
            
            // CityAndTownList
                // CityAndTown
            json.TodoCityAndTownList.allTodos { (result, error) in
                if error == nil {
                    GlobalFunctions.cityAndTownList = result
                    self.open()
                } else {
                    print("Error City", error!)
                }
            }

            // Slider
            json.TodoSliderList.allTodos { (result, error) in
                if error == nil {
                    GlobalFunctions.sliderList = result
                     self.open()
                } else {
                    print("sliderlist", error!)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.label.text = "Bir Hata oluştu.\nDaha sonra tekrar deneyiniz.\nSliderlist \(error!.localizedDescription)"
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.activity.stopAnimating()
                    }
                }
            }
            
        
        } else {
            label.text = "İnternet bağlantınız çevrimdışı görünüyor. Bir ağa bağlanıp tekrar deneyiniz."
            print(label.text!)
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            activity.stopAnimating()
        }
    }

    func open() {
        if GlobalFunctions.sliderList != nil && GlobalFunctions.cityAndTownList != nil {
            self.dismiss(animated: false, completion: nil)
            self.performSegue(withIdentifier: "open", sender: self as AnyObject)
            
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            activity.stopAnimating()
            print("Successfull")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
