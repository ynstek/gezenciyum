//
//  UrunDetayVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 2.12.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class UrunDetayVC: UIViewController, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var desc: UITextView!
        
    @IBOutlet weak var txtUrunDetay: UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    @IBOutlet weak var txtButonQty: UILabel!
    @IBOutlet weak var btnCikar: UIButton!
    
    var qty = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fillProduct()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if GlobalFunctions.productList == nil {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func btnSepeteEkle(_ sender: Any) {
        if GlobalFunctions.currentUser == nil {
            self.openLoginView()
        } else {
            print("Start Produnct add to Card Update.")
            GlobalFunctions.openActivity()
            // TodoShopCardAdd
            json.TodoShopCardAdd.allTodos(productId: GlobalFunctions.selectedProduct!.Id!) { (result, error) in
                
                if error == nil {
                    if result!.sonuc != nil {
                        print("Produnct add to Card Updated.")
                        GlobalFunctions.productList = nil

                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.qty = self.qty + 1
                            if self.qty == 0 {
                                self.btnCikar.isHidden = true
                                self.txtButonQty.text = "Sepete Ekle"
                            } else {
                                self.btnCikar.isHidden = false
                                self.txtButonQty.text = "Sepete Ekle" + " (" + String(describing: self.qty) + ")"
                            }
                            GlobalFunctions.closeActivity()
                        }
                        
                        self.syncCardBadgeValue()

                        
                    } else if let errorMessage = result!.error {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: errorMessage)
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            GlobalFunctions.closeActivity()
                        }
                        AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                    }
                    
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                    }
                    print("Error ", error!)
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                }
            }
        }
    }
    
    @IBAction func btnCikar(_ sender: Any) {
        GlobalFunctions.openActivity()
        print("Start Produnct remove to Card Update.")
        // TodoShopCardRemove
        json.TodoShopCardRemove.allTodos(productId: GlobalFunctions.selectedProduct!.Id!) { (result, error) in
            
            if error == nil {
                if result!.sonuc != nil {
                    print("Produnct remove to Card Updated.")
                    GlobalFunctions.productList = nil
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.qty = self.qty - 1
                        
                        if self.qty == 0 {
                            self.btnCikar.isHidden = true
                            self.txtButonQty.text = "Sepete Ekle"
                        } else {
                            self.btnCikar.isHidden = false
                            self.txtButonQty.text = "Sepete Ekle" + " (" + String(describing: self.qty) + ")"
                        }
                        GlobalFunctions.closeActivity()
                    }
                    
                    self.syncCardBadgeValue()
                    
                } else if let errorMessage = result!.error {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                    }
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: errorMessage)
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.closeActivity()
                    }
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
                }
                
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    GlobalFunctions.closeActivity()
                }
                print("Error ", error!)
                AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: "Bir hata oluştu")
            }
        }
    }
    
    func syncCardBadgeValue() {
        json.TodoCardList.allTodos { (result, error) in
            if error == nil {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    GlobalFunctions.cardList = result
                    // BadgeValue
                    if let tabItems = self.tabBarController?.tabBar.items as NSArray!
                    {
                        let tabItem = tabItems[2] as! UITabBarItem
                        tabItem.badgeValue = String(result!.count)
                    }
                }
            }
        }
    }
    
    func fillProduct () {
        
        var count = ""

        if GlobalFunctions.currentUser != nil {
            qty = GlobalFunctions.selectedProduct!.MyProdQuantity!
            
            if self.qty != 0 {
                count = " (" + String(describing: self.qty) + ")"
                self.btnCikar.isHidden = false
            } else {
                self.btnCikar.isHidden = true
            }
        } else {
            self.btnCikar.isHidden = true
        }
        
        self.txtButonQty.text = "Sepete Ekle" + count
        image.image = nil
        DispatchQueue.main.async {
            self.image.downloadedFrom(link: GlobalFunctions.selectedProduct!.Image!)
            self.activity.stopAnimating()
        }
        
        txtUrunDetay.text = GlobalFunctions.selectedProduct!.Name
        name.text = GlobalFunctions.selectedProduct!.Name
        price.text = String(GlobalFunctions.selectedProduct!.Price!) + " TL"
        desc.text = GlobalFunctions.selectedProduct!.Description
        
        self.btnCikar.setBorderColor()
    }
    
    func openLoginView(){
        let UpdatePopover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login") as! UINavigationController
        UpdatePopover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        UpdatePopover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = UpdatePopover.popoverPresentationController
        popoverCont?.delegate = self // add UIPopoverPresentationControllerDelegate
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(UpdatePopover, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    // Back Trigger
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil {
            // The back button was pressed or interactive gesture used
        }
    }
    
}
