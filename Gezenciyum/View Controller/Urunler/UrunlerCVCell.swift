//
//  UrunlerCVCell.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 2.12.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class UrunlerCVCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var price: UILabel!
    
    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    
    
}
