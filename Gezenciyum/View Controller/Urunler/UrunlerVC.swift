//
//  UrunlerVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 25.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class UrunlerVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        fillProduct()
    }
    
    func fillProduct() {
        if GlobalFunctions.productList == nil {
            self.collectionView.allowsSelection = false
            print("Start Produnct List Update.")
            GlobalFunctions.openActivity()
            // Products
            json.TodoProductList.allTodos { (result, error) in

                if error == nil {
                    print("Produnct List Updated.")
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalFunctions.productList = result
                        self.collectionView.reloadData()
                        self.collectionView.allowsSelection = true
                        GlobalFunctions.closeActivity()

                    }

                } else {
                    print("ProductsList", error!)
                }
            }
        }
    }
    
    // MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = GlobalFunctions.productList?.count {
            return count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as! UrunlerCVCell
        
        cell.image.image = nil
        DispatchQueue.main.async {
            cell.image.downloadedFrom(link: GlobalFunctions.productList![indexPath.row].Image!)
            cell.activity.stopAnimating()
        }
        
        cell.name.text = GlobalFunctions.productList![indexPath.row].Name
        cell.price.text =  String(GlobalFunctions.productList![indexPath.row].Price!) + " TL"
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        let cell = sender as! UICollectionViewCell
//        let indexPath = self.collectionView!.indexPath(for: cell)!

        GlobalFunctions.selectedProduct = GlobalFunctions.productList![indexPath.row]
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let cell = sender as! UICollectionViewCell
//        let indexPath = self.collectionView!.indexPath(for: cell)!

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Back Trigger
    override func willMove(toParentViewController parent: UIViewController?) {
        super.willMove(toParentViewController: parent)
        if parent == nil {
            // The back button was pressed or interactive gesture used
        }
    }

}
