//
//  EskiSiparisCell.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 8.12.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class EskiSiparisCell: UICollectionViewCell {
    
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var OrderStatus: UILabel!
    @IBOutlet weak var orderNo: UILabel!
    
    @IBOutlet weak var orderDesc: UITextView!
    
}
