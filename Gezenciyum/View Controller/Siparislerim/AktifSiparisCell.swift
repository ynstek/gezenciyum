//
//  AktifSiparisCell.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 26.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class AktifSiparisCell: UICollectionViewCell {
    
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var OrderStatus: UILabel!
    @IBOutlet weak var orderNo: UILabel!
    
    @IBOutlet weak var orderDesc: UITextView!
    
}
