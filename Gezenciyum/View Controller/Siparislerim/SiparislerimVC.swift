//
//  HesabimVC.swift
//  Gezenciyum
//
//  Created by Yunus Tek on 25.11.2017.
//  Copyright © 2017 gezenciyum. All rights reserved.
//

import UIKit

class SiparislerimVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIPopoverPresentationControllerDelegate {

    @IBOutlet weak var aktifCollectionView: UICollectionView!
    @IBOutlet weak var eskiCollectionView: UICollectionView!
    
    @IBOutlet weak var lblAktifSiparis: UILabel!
    @IBOutlet weak var lblEskiSiparis: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        presentTransparentNavigationBar()
        NotificationCenter.default.addObserver(self, selector: #selector(SepetVC.reloadCard(_:)), name: NSNotification.Name(rawValue: "reloadCard"), object: nil)
        

        let size: CGFloat = (self.view.frame.width - 290) / 2
        
        let collectionViewLayout = aktifCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        collectionViewLayout?.sectionInset = UIEdgeInsets(top: 0, left: size, bottom: 0, right: size)
        collectionViewLayout?.minimumLineSpacing = size * 2
        
        self.aktifCollectionView.collectionViewLayout = collectionViewLayout!
    }
    
    @objc func reloadCard (_ notification: Notification) {
        getLastOrder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if GlobalFunctions.currentUser != nil {
            getLastOrder()
        } else {
            openLoginView()
        }
    }
    
    func getLastOrder () {
        GlobalFunctions.openActivity()
        self.aktifCollectionView.allowsSelection = false
        self.eskiCollectionView.allowsSelection = false
        
        print("Start Orders Update.")
        // Last Order List
        json.TodoLastOrderList.allTodos { (result, error) in
            if error == nil {
                print("Orders Updated.")
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    GlobalFunctions.lastOrder = result
                    
                    // Array Sort
                    GlobalFunctions.lastOrder?.sort { (a, b) -> Bool in
                        return a.CreateDate! > b.CreateDate!
                    }
                    
                    self.getActiveOrder()
                    self.aktifCollectionView.reloadData()
                    self.aktifCollectionView.allowsSelection = true
                    
                    self.getOldOrder()
                    self.eskiCollectionView.reloadData()
                    self.eskiCollectionView.allowsSelection = true
                    
                    GlobalFunctions.closeActivity()
                }

            } else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    GlobalFunctions.closeActivity()
                    AlertFunctions.messageType.showOKAlert("UYARI", bodyMessage: error!.localizedDescription)
                }
                print("Last Order", error!)
            }
        }
    }
    
    func getActiveOrder() {
        GlobalFunctions.activeOrder = nil
        GlobalFunctions.activeOrder = []
        GlobalFunctions.activeOrder = GlobalFunctions.activeOrder! + GlobalFunctions.lastOrder!.filter() {
            last in
            return (last.OrderStatusId == 1 || last.OrderStatusId == 2)
        }
        
        self.lblAktifSiparis.text = "Aktif Siparişlerim(\(GlobalFunctions.activeOrder!.count))"
        
    }
    
    func getOldOrder() {
        GlobalFunctions.oldOrder = nil
        GlobalFunctions.oldOrder = []
        GlobalFunctions.oldOrder = GlobalFunctions.oldOrder! + GlobalFunctions.lastOrder!.filter() {
            last in
            return (last.OrderStatusId != 1 && last.OrderStatusId != 2)
        }
        
        self.lblEskiSiparis.text = "Eski Siparişlerim(\(GlobalFunctions.oldOrder!.count))"

    }
    
    // MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.aktifCollectionView {
            if let count = GlobalFunctions.activeOrder?.count {
                return count
            }
        } else if collectionView == self.eskiCollectionView {
            if let count = GlobalFunctions.oldOrder?.count {
                return count
            }
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.aktifCollectionView {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "aktifSiparisCell", for: indexPath) as! AktifSiparisCell
            let data = GlobalFunctions.activeOrder!
        
            let datetime = String(data[indexPath.row].CreateDate!).replacingOccurrences(of: "T", with: " ").replacingOccurrences(of: "-", with: ".")
            cell.dateTime.text = String(datetime.prefix(16))
            cell.totalPrice.text = String(data[indexPath.row].TotalPrice!) + " TL"

            cell.orderNo.text = "Sipariş No\n" + data[indexPath.row].OrderCode!
            cell.OrderStatus.text = data[indexPath.row].Name! //+ " >"

            var desc = ""
            for order in data[indexPath.row].orderProducts! {
                let totalPrice = String(Double(order.Quantity!) * order.Price!)
                desc = desc + "(" + totalPrice + " TL) " + String(order.Quantity!) + " x " + order.Name! + "\n"
            }

            cell.orderDesc.text = String(desc.dropLast(1))
        
            return cell
            
        } else { // Eski
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eskiSiparisCell", for: indexPath) as! EskiSiparisCell
            let data = GlobalFunctions.oldOrder!
            
            let datetime = String(data[indexPath.row].CreateDate!).replacingOccurrences(of: "T", with: " ").replacingOccurrences(of: "-", with: ".")
            cell.dateTime.text = String(datetime.prefix(16))
            cell.totalPrice.text = String(data[indexPath.row].TotalPrice!) + " TL"
            
            cell.orderNo.text = "Sipariş No\n" + data[indexPath.row].OrderCode!
            cell.OrderStatus.text = data[indexPath.row].Name!
            
            var desc = ""
            for order in data[indexPath.row].orderProducts! {
                let totalPrice = String(Double(order.Quantity!) * order.Price!)
                desc = desc + "(" + totalPrice + " TL) " + String(order.Quantity!) + " x " + order.Name! + "\n"
            }
            
            cell.orderDesc.text = desc //+ "\nSipariş Durumu:\n" + data[indexPath.row].Name!
            
            return cell
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openLoginView(){
        let UpdatePopover = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Login") as! UINavigationController
        UpdatePopover.modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        UpdatePopover.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        let popoverCont = UpdatePopover.popoverPresentationController
        popoverCont?.delegate = self // add UIPopoverPresentationControllerDelegate
        popoverCont?.permittedArrowDirections = .any
        //        popoverCont?.sourceView = sender as? UIView
        self.present(UpdatePopover, animated: true, completion: nil)
    }
    
    func presentTransparentNavigationBar() {
        self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
        self.navigationController!.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.shadowImage = UIImage()
        self.navigationController!.setNavigationBarHidden(false, animated:true)
        self.navigationItem.hidesBackButton = true
        //        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
        //        self.navigationController!.navigationBar.barTintColor = GlobalFunctions.shared.getColor("black")
    }
    
}
